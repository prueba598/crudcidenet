import React, {useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import axios from 'axios';

function App() {
  const baseUrl="http://localhost/apiCidenet/";
  const [data, setData]=useState([]);
  const [modalInsertar, setModalInsertar]= useState(false);
  const [modalEditar, setModalEditar]= useState(false);
  const [modalEliminar, setModalEliminar]= useState(false);
  const [duplicate, setDuplicate]= useState(false);
  const [errorfecha, setErrorfecha]= useState(false);
  const [empleadoSeleccionado, setempleadoSeleccionado]=useState({
    Emp_ID:'',
    Emp_ApePri:'',
    Emp_ApeSec:'',
    Emp_NomPri:'',
    Emp_NomOtr:'',
    Emp_Pais:'',
    Emp_TipIde:'',
    Emp_NumIde:'',
    Emp_Are:'',
    Emp_FecIng:'',
    Emp_Ema:''
  });
  
  const handleChange=e=>{
    const {name, value}=e.target;
    setempleadoSeleccionado((prevState)=>({
      ...prevState,
      [name]:value
    }))
  }

  const abrirCerrarModalInsertar=()=>{
    setModalInsertar(!modalInsertar);
  }

  const abrirCerrarModalEditar=()=>{
    setModalEditar(!modalEditar);
  }

  const abrirCerrarModalEliminar=()=>{
    setModalEliminar(!modalEliminar);
  }

  const peticionGet=async()=>{
    await axios.get(baseUrl)
    .then(response=>{
      setData(response.data);
    }).catch(error=>{
      console.log(error);
    })
  }
  const validarDuplicado=()=>{
    fetch(baseUrl+'?ideValidate='+empleadoSeleccionado.Emp_NumIde+'&tipValidate='+empleadoSeleccionado.Emp_TipIde+'&ID='+empleadoSeleccionado.Emp_ID)
    .then(respuesta=>respuesta.json())
    .then((datosRespuesta)=>{
        console.log(datosRespuesta.length);
        if (datosRespuesta.length != 0) {
          setDuplicate(true);
        }else{
          setDuplicate(false);
        }
    }).catch(error=>{
      console.log(error);
    })
  }
//Registrar empleado
  const enviarDatos = (e) =>{
    e.preventDefault();
   validarDuplicado();
    if (duplicate === false) {
      var datosEnviar={
        Emp_TipIde:empleadoSeleccionado.Emp_TipIde,
        Emp_NumIde:empleadoSeleccionado.Emp_NumIde,
        Emp_ApePri:empleadoSeleccionado.Emp_ApePri,
        Emp_ApeSec:empleadoSeleccionado.Emp_ApeSec,
        Emp_NomPri:empleadoSeleccionado.Emp_NomPri,
        Emp_NomOtr:empleadoSeleccionado.Emp_NomOtr,
        Emp_Pais:empleadoSeleccionado.Emp_Pais,
        Emp_Are:empleadoSeleccionado.Emp_Are,
        Emp_FecIng:empleadoSeleccionado.Emp_FecIng
      }
      fetch(baseUrl+"?METHOD=POST",{
          method:"POST",
          body:JSON.stringify(datosEnviar)
      })
      .then(respuesta=>respuesta.json())
      .then((datosRespuesta)=>{
          console.log(datosRespuesta);
      }).catch(error=>{
        console.log(error);
      }) 
    }
    peticionGet();
    abrirCerrarModalInsertar();
  }

//Actualizar empleado
  const actualizarDatos = (e) =>{
    e.preventDefault();
    validarDuplicado();
    if (duplicate === false) {
      var datosActualizar={
        Emp_ID:empleadoSeleccionado.Emp_ID,
        Emp_TipIde:empleadoSeleccionado.Emp_TipIde,
        Emp_NumIde:empleadoSeleccionado.Emp_NumIde,
        Emp_ApePri:empleadoSeleccionado.Emp_ApePri,
        Emp_ApeSec:empleadoSeleccionado.Emp_ApeSec,
        Emp_NomPri:empleadoSeleccionado.Emp_NomPri,
        Emp_NomOtr:empleadoSeleccionado.Emp_NomOtr,
        Emp_Pais:empleadoSeleccionado.Emp_Pais,
        Emp_Are:empleadoSeleccionado.Emp_Are,
        Emp_FecIng:empleadoSeleccionado.Emp_FecIng
      }
      console.log(datosActualizar);
      fetch(baseUrl+"?METHOD=PUT",{
          method:"POST",
          body:JSON.stringify(datosActualizar)
      })
      .then(respuesta=>respuesta.json())
      .catch(error=>{
        console.log(error);
      }) 
    }
    peticionGet();
    abrirCerrarModalEditar();
  }
//Eliminar empleado
  const peticionDelete=async()=>{
    var f = new FormData();
    f.append("METHOD", "DELETE");
    await axios.post(baseUrl, f, {params: {Emp_ID: empleadoSeleccionado.Emp_ID}})
    .then(response=>{
      setData(data.filter(empleado=>empleado.Emp_ID!==empleadoSeleccionado.Emp_ID));
      abrirCerrarModalEliminar();
    }).catch(error=>{
      console.log(error);
    })
  }
//Consultar empleados
  const seleccionarEmpleado=(empleado, caso)=>{
    setempleadoSeleccionado(empleado);

    (caso==="Editar")?
    abrirCerrarModalEditar():
    abrirCerrarModalEliminar()
  }

  useEffect(()=>{
    peticionGet();
  },[])

  return (
    <div style={{textAlign: 'center'}}>
      {duplicate==true && <div className="alert alert-danger alert-dismissible fade show" role="alert">
      <strong>Registro fallido</strong> Ya existe un empleado con el mismo tipo y numero de identidad!
        <button type="button" className="btn-close" onClick={()=>setDuplicate(false)} data-bs-dismiss="alert" aria-label="Close"></button>
      </div>}
      <br />
      <button className="btn btn-success" onClick={()=>abrirCerrarModalInsertar()}>Insertar</button>
      <br /><br />
      <table className="table table-striped">
        <thead>
          <tr>
          <th>Tipo Identificacion</th>
            <th>Identificacion</th>
            <th>Nombre</th>
            <th>Correo electronico</th>
            <th>Area</th>
            <th>Pais</th>
            <th>Fecha de ingreso</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          {data.map(empleado=>(
            <tr key={empleado.Emp_ID}>
              <td>{empleado.Emp_TipIde}</td>
              <td>{empleado.Emp_NumIde}</td>
              <td>{empleado.Emp_NomPri} {empleado.Emp_NomOtr} {empleado.Emp_ApePri} {empleado.Emp_ApeSec}</td>
              <td>{empleado.Emp_Ema}</td>
              <td>{empleado.Emp_Are}</td>
              <td>{empleado.Emp_Pais}</td>
              <td>{empleado.Emp_FecIng}</td>
            <td>
            <button className="btn btn-primary" onClick={()=>seleccionarEmpleado(empleado, "Editar")}>Editar</button> {"  "}
            <button className="btn btn-danger" onClick={()=>seleccionarEmpleado(empleado, "Eliminar")}>Eliminar</button>
            </td>
            </tr>
          ))}
        </tbody>
      </table>

      <Modal className="modal-lg" isOpen={modalInsertar}>
        <ModalHeader>Insertar Empleado</ModalHeader>
        <ModalBody>
          <form onSubmit={enviarDatos}>
          <div className="form-group">
            <div className="row">
                <div className="form-group col-md-3">
                    <label>Primer Nombre: </label>
                    <input type="text" name="Emp_NomPri"  onChange={handleChange} id="pname" className="form-control" pattern='[^Ñ][A-Z]{1,20}' required/>
                </div>
                <div className="form-group col-md-3">
                    <label>Otros Nombres: </label>
                    <input type="text" name="Emp_NomOtr"  onChange={handleChange} id="oname" className="form-control" pattern='[^Ñ][A-Z]{1,50}' />
                </div>                            
                <div className="form-group col-md-3">
                    <label>Primer Apellido: </label>
                    <input type="text" name="Emp_ApePri"  onChange={handleChange} id="pape" className="form-control" pattern='[^Ñ][A-Z]{1,20}' required />
                </div>
                <div className="form-group col-md-3">
                    <label>Segundo Apellido: </label>
                    <input type="text" name="Emp_ApeSec"  onChange={handleChange} id="sape" className="form-control" pattern='[^Ñ][A-Z]{1,20}' required />
                </div>
            </div><br></br>
            <div className="row">
                <div className="form-group col-md-3">
                    <label>País del empleo: </label>
                    <select name="Emp_Pais"  onChange={handleChange} className="form-select" required>
                        <option value="">Seleccione</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Estados Unidos">Estados Unidos</option>
                    </select>
                </div>
                <div className="form-group col-md-3">
                    <label>Tipo de Identificación: </label>
                    <select name="Emp_TipIde"  onChange={handleChange} className="form-select" required>
                        <option value="">Seleccione</option> 
                        <option value="Cédula de Ciudadanía">Cédula de Ciudadanía</option>                                    
                        <option value="Cédula de Extranjería">Cédula de Extranjería</option>
                        <option value="Pasaporte">Pasaporte</option>
                        <option value="Permiso Especial">Permiso Especial</option>
                    </select>
                </div>                            
                <div className="form-group col-md-3">
                    <label>No. de Identificación: </label>
                    <input type="text" name="Emp_NumIde"  onChange={handleChange} id="nide" className="form-control" pattern='[A-z/0-9/-]{1,20}' required />
                </div>                            
                <div className="form-group col-md-3">
                    <label>Área: </label>
                    <select name="Emp_Are"  onChange={handleChange} className="form-select" required>
                    <option value="">Seleccione</option>
                    <option value="Administración">Administración</option>
                    <option value="Financiera">Financiera</option>
                    <option value="Compras">Compras</option>
                    <option value="Infraestructura">Infraestructura</option>
                    <option value="Operación">Operación</option>
                    <option value="Talento Humano">Talento Humano</option>
                    <option value="Servicios Varios">Servicios Varios</option>
                    </select>
                </div>
            </div><br></br>
            <div className="row">
                <div className="form-group col-md-3">
                    <label>Fecha de Ingreso: </label>
                    <input type="date" name="Emp_FecIng"  onChange={handleChange} id="fing" className="form-control" required />
                </div>
            </div><br></br>
          </div>
            <button type="submit" className="btn btn-success">Guardar</button>{"  "}
            <a className="btn btn-danger" onClick={()=>abrirCerrarModalInsertar()}>Cancelar</a>
          </form>
        </ModalBody>
        <ModalFooter>
        
        </ModalFooter>
      </Modal>
 
      <Modal className="modal-lg" isOpen={modalEditar}>
        <ModalHeader>Editar Empleado</ModalHeader>
        <ModalBody>
          <form onSubmit={actualizarDatos}>
            <div className="form-group">
              <div className="row">
                  <div className="form-group col-md-3">
                      <label>Primer Nombre: </label>
                      <input type="text" name="Emp_NomPri" onChange={handleChange} className="form-control"  value={empleadoSeleccionado && empleadoSeleccionado.Emp_NomPri} pattern='[^Ñ][A-Z]{1,20}'  required/>
                  </div>
                  <div className="form-group col-md-3">
                      <label>Otros Nombres: </label>
                      <input type="text" name="Emp_NomOtr" onChange={handleChange} className="form-control"  value={empleadoSeleccionado && empleadoSeleccionado.Emp_NomOtr} pattern='[^Ñ][A-Z]{1,50}' />
                  </div>                            
                  <div className="form-group col-md-3">
                      <label>Primer Apellido: </label>
                      <input type="text" name="Emp_ApePri" onChange={handleChange} className="form-control"  value={empleadoSeleccionado && empleadoSeleccionado.Emp_ApePri} pattern='[^Ñ][A-Z]{1,20}' required/>
                  </div>
                  <div className="form-group col-md-3">
                      <label>Segundo Apellido: </label>
                      <input type="text" name="Emp_ApeSec" onChange={handleChange} className="form-control"  value={empleadoSeleccionado && empleadoSeleccionado.Emp_ApeSec} pattern='[^Ñ][A-Z]{1,20}' required/>
                  </div>
              </div><br></br>
              <div className="row">
                  <div className="form-group col-md-3">
                      <label>País del empleo: </label>
                      <select name="Emp_Pais" onChange={handleChange} className="form-select" value={empleadoSeleccionado && empleadoSeleccionado.Emp_Pais} required>
                          <option value="">Seleccione</option>
                          <option value="Colombia">Colombia</option>
                          <option value="Estados Unidos">Estados Unidos</option>
                      </select>
                  </div>
                  <div className="form-group col-md-3">
                      <label>Tipo de Identificación: </label>
                      <select name="Emp_TipIde" onChange={handleChange} className="form-select" value={empleadoSeleccionado && empleadoSeleccionado.Emp_TipIde} required>
                          <option value="">Seleccione</option> 
                          <option value="Cédula de Ciudadanía">Cédula de Ciudadanía</option>                                    
                          <option value="Cédula de Extranjería">Cédula de Extranjería</option>
                          <option value="Pasaporte">Pasaporte</option>
                          <option value="Permiso Especial">Permiso Especial</option>
                      </select>
                  </div>                            
                  <div className="form-group col-md-3">
                      <label>No. de Identificación: </label>
                      <input type="text" name="Emp_NumIde" onChange={handleChange} className="form-control"  value={empleadoSeleccionado && empleadoSeleccionado.Emp_NumIde} pattern='[A-z/0-9/-]{1,20}'  required />
                  </div>                             
                  <div className="form-group col-md-3">
                      <label>Área: </label>
                      <select name="Emp_Are" onChange={handleChange} className="form-select" value={empleadoSeleccionado && empleadoSeleccionado.Emp_Are} required>
                        <option value="">Seleccione</option>
                        <option value="Administración">Administración</option>
                        <option value="Financiera">Financiera</option>
                        <option value="Compras">Compras</option>
                        <option value="Infraestructura">Infraestructura</option>
                        <option value="Operación">Operación</option>
                        <option value="Talento Humano">Talento Humano</option>
                        <option value="Servicios Varios">Servicios Varios</option>
                      </select>
                  </div>
              </div><br></br>
              <div className="row">
                  <div className="form-group col-md-3">
                      <label>Fecha de Ingreso: </label>
                      <input type="date" name="Emp_FecIng" onChange={handleChange} className="form-control"  value={empleadoSeleccionado && empleadoSeleccionado.Emp_FecIng}/>
                  </div>
              </div><br></br>
              <button type="submit" className="btn btn-success">Editar</button>{"  "}
              <a className="btn btn-danger" onClick={()=>abrirCerrarModalEditar()}>Cancelar</a>
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          
        </ModalFooter>
      </Modal>

      <Modal isOpen={modalEliminar}>
        <ModalBody>
        ¿Estás seguro que deseas eliminar el Empleado {empleadoSeleccionado && empleadoSeleccionado.Emp_NomPri}?
        </ModalBody>
        <ModalFooter>
          <button className="btn btn-danger" onClick={()=>peticionDelete()}>
            Sí
          </button>
          <button
            className="btn btn-secondary"
            onClick={()=>abrirCerrarModalEliminar()}
          >
            No
          </button>
        </ModalFooter>
      </Modal>

    </div>
  );
}

export default App;